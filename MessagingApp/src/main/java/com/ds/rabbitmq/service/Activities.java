package com.ds.rabbitmq.service;

import com.ds.rabbitmq.model.MonitoredData;
import org.joda.time.Duration;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Activities {

//    public Map<Integer, Map<String, List<Long>>> getData() throws IOException, ParseException {
//    public Map<String, List<Long>> getData() throws IOException, ParseException {
    public List<MonitoredData> getData() throws IOException, ParseException {



    List<MonitoredData> monitoredData = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try (Stream<String> stream = Files.lines(Paths.get("activity.txt"))) {
            String[] data;
            Iterator<String> iterator = stream.iterator();

            while (iterator.hasNext()) {
                data = iterator.next().split("\\s{2,}");
                MonitoredData m = new MonitoredData();
                m.setStartTime(dateFormat.parse(data[0]));
                m.setEndTime(dateFormat.parse(data[1]));
                m.setLabel(data[2]);
                monitoredData.add(m);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Map<Integer, Map<String, List<Long>>> durationOfActivities = monitoredData.stream()
                .collect(Collectors.groupingBy(a -> a.getStartTimeDay(),
                        Collectors.groupingBy(y -> y.getLabel(), Collectors.mapping(z -> z.getDuration(), Collectors.toList())))
                );

        Map<String, List<Long>> listDurationOfActivities = monitoredData.stream()
                .collect(Collectors.groupingBy(y -> y.getLabel(), Collectors.mapping(z -> z.getDuration(), Collectors.toList())));

        for (Entry<Integer,  Map<String, List<Long>>> entry : durationOfActivities.entrySet()) {
            System.out.println(entry.getKey());
            for (Entry<String, List<Long>> entry2 : entry.getValue().entrySet()) {
                System.out.println(entry2.getKey());
                for (Long l : entry2.getValue()) {
                    Duration duration = new Duration(Math.abs(l));
                    System.out.println(duration.toPeriod().getHours() + "H " + duration.toPeriod().getMinutes() + "M " + duration.toPeriod().getSeconds() + "S");
                    System.out.println();
                }
            }
        }

//        return durationOfActivities;
        return monitoredData;

    }
}
