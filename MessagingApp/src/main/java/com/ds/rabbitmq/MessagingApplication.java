package com.ds.rabbitmq;

import com.ds.rabbitmq.model.MonitoredData;
import com.ds.rabbitmq.service.Activities;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;
import java.util.Map;

@SpringBootApplication
public class MessagingApplication {

	private static final boolean NON_DURABLE = false;
	private static final String QUEUE_NAME = "activity";

	public static void main(String[] args) {
		SpringApplication.run(MessagingApplication.class, args);
	}

	@Bean
	public ApplicationRunner runner(RabbitTemplate template) {
		return args -> {
			Activities a = new Activities();
//			Map<Integer, Map<String, List<Long>>> activities = a.getData();
// 			Map<String, List<Long>> activities = a.getData();
			List<MonitoredData> activities = a.getData();
			String message = "";
//			for (Map.Entry<Integer,  Map<String, List<Long>>> entry : activities.entrySet()) {
//				for (Map.Entry<String, List<Long>> entry2 : entry.getValue().entrySet()) {
//					for (Long l : entry2.getValue()) {
//						message = "{\"patientId\":\"3\",\"activity\":\"" + entry2.getKey().replaceAll("\\s","") + "\",\"duration\":\"" + l + "\"}";
//						template.convertAndSend("activity", message);
//						System.out.println("[x] Sent '"  + message + "'");
//						Thread.sleep(1000);
//					}
//				}
//			}
//			for (Map.Entry<String, List<Long>> entry: activities.entrySet()) {
//				for (Long l : entry.getValue()) {
//					message = "{\"patientId\":\"3\",\"activity\":\"" + entry.getKey().replaceAll("\\s","") + "\",\"duration\":\"" + l + "\"}";
//					template.convertAndSend("activity", message);
//					System.out.println("[x] Sent: " + message);
//					Thread.sleep(1000);
//				}
//			}
			for (MonitoredData m : activities) {
				message = "{\"patientId\":\"3\",\"activity\":\"" + m.getLabel().replaceAll("\\s","") + "\",\"duration\":\"" + m.getDuration() + "\"}";
				template.convertAndSend("activity", message);
				System.out.println("[x] Sent: " + message);
				Thread.sleep(1000);
			}
		};
	}

	@Bean
	public Queue activity() {
		return new Queue(QUEUE_NAME, NON_DURABLE);
	}
}
