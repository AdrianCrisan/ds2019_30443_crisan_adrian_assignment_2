package com.ds.rabbitmq.model;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.Date;

public class MonitoredData {

    private Date startTime;
    private Date endTime;
    private String label;

    public MonitoredData() {
    }

    public Date getStartTime() {
        return startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getStartTimeDay() {
        return startTime.getDate();
    }

    public Long getDuration() {

        DateTime x = new DateTime(this.getEndTime());
        DateTime y = new DateTime(this.getStartTime());

        Duration z = new Duration(y,x);

        return Math.abs(z.getMillis());
    }
}
