package com.ds.lab.service;

import com.ds.lab.model.*;
import com.ds.lab.model.dto.*;
import com.ds.lab.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.*;

@Service
public class DoctorService {

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private CaretakerRepository caretakerRepository;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private MedicationRepository medicationRepository;

    @Autowired
    private MedicationPlanRepository medicationPlanRepository;

    public List<PatientDTO> getAllPatients(String username) {
        Doctor doctor = doctorRepository.findByUsername(username);
        List<Patient> patients = patientRepository.findAllPatientsOfDoctor(doctor.getId());
        List<PatientDTO> patientDTOS = new ArrayList<>();

        for (Patient p: patients) {
            PatientDTO dto = new PatientDTO();
            dto.setId(p.getId());
            dto.setName(p.getName());
            dto.setAddress(p.getAddress());
            dto.setBirthDate(p.getBirthDate().toString());
            dto.setGender(p.getGender().toString());
            patientDTOS.add(dto);
        }
        return patientDTOS;
    }

    public Patient getPatientById(long id) {
        return patientRepository.findById(id).get();
    }

    public Patient createPatient(String username, PatientIn patient) {
        Patient patientToAdd = new Patient();
        Date birthDate = Date.valueOf(patient.getBirthDate());
        Caretaker c = caretakerRepository.findByUsername(patient.getCaretaker());
        Doctor d = doctorRepository.findByUsername(username);

        String patientUsername = patient.getName().replaceAll(" ", ".");

        patientToAdd.setRole(RoleName.ROLE_PATIENT);
        patientToAdd.setUsername(patientUsername);
        patientToAdd.setPassword(new BCryptPasswordEncoder().encode("123456"));
        patientToAdd.setName(patient.getName());
        patientToAdd.setBirthDate(birthDate);
        patientToAdd.setAddress(patient.getAddress());
        patientToAdd.setCaretaker(c);
        patientToAdd.setDoctor(d);
        if (patient.getGender().equals("F")) {
            patientToAdd.setGender(Gender.GENDER_F);
        } else {
            patientToAdd.setGender(Gender.GENDER_M);
        }
        c.addPatient(patientToAdd);
        d.addPatient(patientToAdd);
        return patientRepository.save(patientToAdd);
    }

    public Patient updatePatient(Long id, Patient patient) {
        Patient p = patientRepository.findById(id).get();

        p.setAddress(patient.getAddress());
        p.setName(patient.getName());

        return patientRepository.save(p);
    }

    public Map<String, Boolean> deletePatient(Long id) {
        Patient p = patientRepository.findById(id).get();
        patientRepository.delete(p);

        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    public Caretaker getCaretakerById(long id) { return caretakerRepository.findById(id).get(); }

    public List<CaretakerDTO> getAllCaretakers(String username) {
        Doctor doctor = doctorRepository.findByUsername(username);
        List<Caretaker> caretakers = caretakerRepository.findAllCaretakersOfDoctor(doctor.getId());
        List<CaretakerDTO> caretakerDTOS = new ArrayList<>();

        for (Caretaker c: caretakers) {
            CaretakerDTO dto = new CaretakerDTO();
            dto.setId(c.getId());
            dto.setName(c.getName());
            dto.setAddress(c.getAddress());
            dto.setBirthDate(c.getBirthDate().toString());
            dto.setGender(c.getGender().toString());
            caretakerDTOS.add(dto);
        }
        return caretakerDTOS;
    }

    public Caretaker createCaretaker(String username, CaretakerIn caretaker) {
        Caretaker toAdd = new Caretaker();
        Date birthDate = Date.valueOf(caretaker.getBirthDate());
        Doctor d = doctorRepository.findByUsername(username);

        toAdd.setRole(RoleName.ROLE_CARETAKER);
        toAdd.setUsername(caretaker.getName());
        toAdd.setPassword(new BCryptPasswordEncoder().encode(caretaker.getName()));
        toAdd.setName(caretaker.getName());
        toAdd.setBirthDate(birthDate);
        toAdd.setAddress(caretaker.getAddress());
        toAdd.setDoctor(d);
        if (caretaker.getGender().equals("F")) {
            toAdd.setGender(Gender.GENDER_F);
        } else {
            toAdd.setGender(Gender.GENDER_M);
        }
        d.addCaretaker(toAdd);
        return caretakerRepository.save(toAdd);
    }

    public Medication createMedication(MedicationIn medicationIn) {
        Medication toAdd = new Medication();

        toAdd.setName(medicationIn.getName());
        toAdd.setSideEffects(medicationIn.getSideEffects());
        toAdd.setIntakePerDay(Integer.parseInt(medicationIn.getIntakePerDay()));

        return medicationRepository.save(toAdd);
    }

    public Caretaker updateCaretaker(Long id, Caretaker caretaker) {
        Caretaker c = caretakerRepository.findById(id).get();

        c.setAddress(caretaker.getAddress());
        c.setName(caretaker.getName());

        return caretakerRepository.save(c);
    }

    public Map<String, Boolean> deleteCaretaker(Long id) {
        Caretaker c = caretakerRepository.findById(id).get();
        caretakerRepository.delete(c);

        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    public Map<String, Boolean> deleteMedication(Long id) {
        Medication m = medicationRepository.findById(id).get();
        medicationRepository.delete(m);

        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    public List<MedicationDTO> getAllMedications() {
        List<Medication> medications = medicationRepository.findAll();
        List<MedicationDTO> medicationDTOS = new ArrayList<>();

        for (Medication m : medications) {
            MedicationDTO dto = new MedicationDTO();
            dto.setId(m.getId());
            dto.setName(m.getName());
            dto.setSideEffects(m.getSideEffects());
            dto.setIntakePerDay(String.valueOf(m.getIntakePerDay()));
            medicationDTOS.add(dto);
        }

        return medicationDTOS;
    }

    public MedicationPlan createMedicationPlan(String patientId, MedicationPlanIn medicationPlan) {
        Patient patient = patientRepository.findById(Long.parseLong(patientId)).get();
        MedicationPlan toAdd = new MedicationPlan();

        toAdd.setTreatmentPeriod(medicationPlan.getTreatmentPeriod());
        toAdd.setPatient(patient);
        for (MedicationIn medicationIn : medicationPlan.getMedications()) {
            Medication m = medicationRepository.findByName(medicationIn.getName());
            toAdd.addMedication(m);
        }
        patient.addMedicationPlan(toAdd);
        MedicationPlan m = medicationPlanRepository.save(toAdd);
        patientRepository.save(patient);
        return m;
    }
}
