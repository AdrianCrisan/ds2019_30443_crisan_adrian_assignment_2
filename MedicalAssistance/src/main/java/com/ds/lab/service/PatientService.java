package com.ds.lab.service;

import com.ds.lab.model.Activities;
import com.ds.lab.model.Medication;
import com.ds.lab.model.MedicationPlan;
import com.ds.lab.model.Patient;
import com.ds.lab.model.dto.MedicationDTO;
import com.ds.lab.model.dto.MedicationPlanDTO;
import com.ds.lab.model.dto.PatientDTO;
import com.ds.lab.repository.ActivitiesRepository;
import com.ds.lab.repository.MedicationPlanRepository;
import com.ds.lab.repository.MedicationRepository;
import com.ds.lab.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PatientService {

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private MedicationPlanRepository medicationPlanRepository;

    @Autowired
    private MedicationRepository medicationRepository;

    public PatientDTO getPatientInfo(String username) {
        Patient p = patientRepository.findByUsername(username);

        PatientDTO patientDTO = new PatientDTO();
        patientDTO.setId(p.getId());
        patientDTO.setName(p.getName());
        patientDTO.setAddress(p.getAddress());
        patientDTO.setBirthDate(p.getBirthDate().toString());
        patientDTO.setGender(p.getGender().toString());
        patientDTO.setMedicationPlans(getMedicationPlansOfPatient(username));

        return patientDTO;
    }

    private List<MedicationPlanDTO> getMedicationPlansOfPatient(String username) {
        Patient p = patientRepository.findByUsername(username);
        List<MedicationPlan> medicationPlans = medicationPlanRepository.findMedicationPlansOfPatient(p.getId());

        List<MedicationPlanDTO> medicationPlanDTOS = new ArrayList<>();

        for (MedicationPlan m : medicationPlans) {
            List<MedicationDTO> medicationDTOS = new ArrayList<>();
            MedicationPlanDTO dto = new MedicationPlanDTO();
            dto.setTreatmentPeriod(m.getTreatmentPeriod());
            List<Medication> meds = medicationRepository.getMedicationOfMedicationPlan(m.getId());
            for (Medication medication : meds) {
                MedicationDTO medicationDTO = new MedicationDTO();
                medicationDTO.setName(medication.getName());
                medicationDTO.setSideEffects(medication.getSideEffects());
                medicationDTO.setIntakePerDay(String.valueOf(medication.getIntakePerDay()));
                medicationDTOS.add(medicationDTO);
            }
            dto.setReceivedMedications(medicationDTOS);
            medicationPlanDTOS.add(dto);
        }
        return medicationPlanDTOS;
    }

}
