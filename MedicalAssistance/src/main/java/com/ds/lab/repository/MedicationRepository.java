package com.ds.lab.repository;

import com.ds.lab.model.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, Long> {

    Medication findByName(String name);

    @Query(value = "SELECT * FROM medication AS m INNER JOIN medication_medicationplan AS mmp ON " +
            "m.id = mmp.medication_id INNER JOIN medication_plan AS mp ON " +
            "mmp.medication_plan_id = mp.id WHERE mp.id = ?1", nativeQuery = true)
    List<Medication> getMedicationOfMedicationPlan(Long id);
}
