package com.ds.lab.repository;

import com.ds.lab.model.Doctor;

import javax.transaction.Transactional;

@Transactional
public interface DoctorRepository extends UserRepository<Doctor>{

    Doctor findByUsername(String username);
}
