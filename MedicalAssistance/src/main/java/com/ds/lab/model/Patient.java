package com.ds.lab.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.List;

@Entity
public class Patient extends User {

    @ManyToOne(
            fetch = FetchType.LAZY
    )
    @JoinColumn(name = "caretaker_id")
    @JsonBackReference(value = "caretaker-patients")
    private Caretaker caretaker;

    @ManyToOne(
            fetch = FetchType.LAZY
    )
    @JoinColumn(name = "doctor_id")
    @JsonBackReference(value = "doctor-patients")
    private Doctor doctor;

    @OneToMany(
            mappedBy = "patient",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<MedicationPlan> medicationPlans;

    @OneToMany(
            fetch = FetchType.EAGER,
            mappedBy = "patient",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<Activities> activities;

    public Patient() {
    }

    public List<Activities> getActivities() {
        return activities;
    }

    public void setActivities(List<Activities> activities) {
        this.activities = activities;
    }

    public void addActivity(Activities activity) {
        activities.add(activity);
        activity.setPatient(this);
    }

    public void addMedicationPlan(MedicationPlan medicationPlan) {
        medicationPlans.add(medicationPlan);
        medicationPlan.setPatient(this);
    }

    public Caretaker getCaretaker() {
        return caretaker;
    }

    public void setCaretaker(Caretaker caretaker) {
        this.caretaker = caretaker;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public List<MedicationPlan> getMedicationPlans() {
        return medicationPlans;
    }

    public void setMedicationPlans(List<MedicationPlan> medicationPlans) {
        this.medicationPlans = medicationPlans;
    }
}
