package com.ds.lab.model.dto;

import com.ds.lab.model.Medication;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class MedicationPlanIn {

    private int treatmentPeriod;

    @JsonProperty("medications")
    private List<MedicationIn> medications;

    public MedicationPlanIn() {
    }

    public int getTreatmentPeriod() {
        return treatmentPeriod;
    }

    public void setTreatmentPeriod(int treatmentPeriod) {
        this.treatmentPeriod = treatmentPeriod;
    }

    public List<MedicationIn> getMedications() {
        return medications;
    }

    public void setMedications(List<MedicationIn> medications) {
        this.medications = medications;
    }
}
