package com.ds.lab.model.dto;

public class MedicationIn {

    private String name;
    private String sideEffects;
    private String intakePerDay;

    public MedicationIn() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getIntakePerDay() {
        return intakePerDay;
    }

    public void setIntakePerDay(String intakePerDay) {
        this.intakePerDay = intakePerDay;
    }
}
