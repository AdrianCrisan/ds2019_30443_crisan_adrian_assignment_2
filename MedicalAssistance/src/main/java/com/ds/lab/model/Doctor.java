package com.ds.lab.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Doctor extends User {

    @OneToMany(
            mappedBy = "doctor",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonManagedReference(value = "doctor-patients")
    private List<Patient> patients = new ArrayList<>();


    @OneToMany(
            mappedBy = "doctor",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonManagedReference(value = "doctor-caretakers")
    private List<Caretaker> caretakers = new ArrayList<>();

    public Doctor() {
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }

    public List<Caretaker> getCaretakers() {
        return caretakers;
    }

    public void setCaretakers(List<Caretaker> caretakers) {
        this.caretakers = caretakers;
    }

    public void addPatient(Patient patient) {
        patients.add(patient);
        patient.setDoctor(this);
    }

    public void removePatient(Patient patient) {
        patients.remove(patient);
        patient.setDoctor(null);
    }

    public void addCaretaker(Caretaker caretaker) {
        caretakers.add(caretaker);
        caretaker.setDoctor(this);
    }

    public void removeCaretaker(Caretaker caretaker) {
        caretakers.remove(caretaker);
        caretaker.setDoctor(null);
    }
}
