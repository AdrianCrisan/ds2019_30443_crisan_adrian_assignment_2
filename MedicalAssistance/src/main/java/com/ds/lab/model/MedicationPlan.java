package com.ds.lab.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class MedicationPlan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "medication_medicationplan",
        joinColumns = @JoinColumn(name = "medication_plan_id"),
        inverseJoinColumns = @JoinColumn(name = "medication_id")
    )
    private Set<Medication> medications = new HashSet<>();

    @ManyToOne(
            fetch = FetchType.LAZY
    )
    @JoinColumn(name = "patient_id")
    private Patient patient;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    private long treatmentPeriod;

    public long getTreatmentPeriod() {
        return treatmentPeriod;
    }

    public void setTreatmentPeriod(long treatmentPeriod) {
        this.treatmentPeriod = treatmentPeriod;
    }

    public Set<Medication> getMedications() {
        return medications;
    }

    public void setMedications(Set<Medication> medications) {
        this.medications = medications;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void addMedication(Medication medication) {
        medications.add(medication);
        medication.getMedicationPlans().add(this);
    }

    public void removeMedication(Medication medication) {
        medications.remove(medication);
        medication.getMedicationPlans().remove(this);
    }
}
