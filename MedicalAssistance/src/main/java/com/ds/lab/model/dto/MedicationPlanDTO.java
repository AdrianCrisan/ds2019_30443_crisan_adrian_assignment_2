package com.ds.lab.model.dto;

import java.util.List;

public class MedicationPlanDTO {

    private long treatmentPeriod;
    private List<MedicationDTO> receivedMedications;

    public MedicationPlanDTO() {
    }

    public long getTreatmentPeriod() {
        return treatmentPeriod;
    }

    public void setTreatmentPeriod(long treatmentPeriod) {
        this.treatmentPeriod = treatmentPeriod;
    }

    public List<MedicationDTO> getReceivedMedications() {
        return receivedMedications;
    }

    public void setReceivedMedications(List<MedicationDTO> receivedMedications) {
        this.receivedMedications = receivedMedications;
    }

}
